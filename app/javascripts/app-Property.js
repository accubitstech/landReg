// Import libraries we need.
import Web3 from 'web3';
import contract from 'truffle-contract';
//Import/Set logger 
var loggerReq = require('./config/logger.js')
var logger=loggerReq.loggerProperty;
//To config host & port
const config = require('../../config.js');
const proCollection = require('../../property.js');

// Import our contract artifacts and turn them into usable abstractions.
import PropertyTransfer_artifacts from '../../build/contracts/PropertyTransfer.json';


var PropertyTransfer = contract(PropertyTransfer_artifacts);


// The following code is simple to show off interacting with your contracts.
// As your needs grow you will likely need to change its form and structure.
// For application bootstrapping, check out window.addEventListener below.
//var accounts;
var account;
var web3;

var Tx = require('ethereumjs-tx');
const keythereum = require("keythereum");

var App = {
      start: function () {
              var self = this;

              web3 = new Web3(new Web3.providers.HttpProvider("http://"+config.development.host+":"+config.development.port));
              
              //web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
              // Bootstrap the GridToken abstraction for Use.
              PropertyTransfer.setProvider(web3.currentProvider);
             
              // Get the initial account balance so it can be displayed.
              web3.eth.getAccounts(function (err, accs) {
                  
                   if (err != null) {
                        logger.error("There was an error fetching your accounts.-E  "+err);
                        //alert("There was an error fetching your accounts.");
                        return;
                   }
                  if (accs.length == 0) {
                       logger.error("Couldn't get any accounts! Make sure your Ethereum client is configured correctly. "+err);
                       //alert("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.");
                       return;
                  }
                    //accounts = accs;
                    // account = accounts[0];
                    account=web3.eth.coinbase;
             });
    },
     //Function for getting count of Users
     getUserCount: function(res) {
          PropertyTransfer.deployed().then(function (instance) {
                      return instance.getUserCount.call();
          }).then(function (result) {
                  res({
                        "status_cd": 200,
                        "data" : {
                                "count":result
                        }
                   });
                   logger.info("getUserCount:SUCCESS");
          }).catch(function (e) {
                   res({
                           "status_cd": 400,
                           "error" : {
                                 "error_cd" : 400,
                                 "message" : "Error in fetching the user count"
                           }
                    });
                    console.log(e);
                    logger.error("getUserCount: ERROR-Error in fetching the user count  "+e);
                    //console.log("Error in fetching the user count");
        });
    },
    //Function for getting count of Properties
    getPropertyCount: function (res) {
             PropertyTransfer.deployed().then(function (instance) {
                          return instance.getPropertyCount.call();
             }).then(function (result) {
                          res({
                               "status_cd": 200,
                               "data" : {
                                       "count":result
                                }
                          });
                          logger.info("getPropertyCount:SUCCESS");
             }).catch(function (e) {
                          res({
                                "status_cd": 400,
                                "error" : {
                                         "error_cd" : 400,
                                         "message" : "Error in fetching the property count"
                                  }
                          });
                         console.log(e);
                         logger.error("getPropertyCount: ERROR-Error in fetching the property count  "+e);
                         //console.log("Error in fetching the property count");
            });
    },
    //Function for verifying the user identity
    isUser: function (req,res) {
            var userId = req.userId;
            //Validation
            if(!userId){
                   res({
                        "status_cd": 400,
                        "error" : {
                              "error_cd" : 400,
                              "message" : "Please enter a valid user Id"
                        }
                   });
                  return;
            }
           PropertyTransfer.deployed().then(function (instance) {
                   return instance.isUser.call(userId);
           }).then(function (result) {
                   res({
                        "status_cd": 200,
                        "data" : {
                                "message":result
                         }
                   });
                   logger.info("isUser:SUCCESS");
           }).catch(function (e) {
                   res({
                         "status_cd": 400,
                         "error" : {
                                  "error_cd" : 400,
                                  "message" : "Error in verifying user identity"
                           }
                   });
                  console.log(e);
                  logger.error("isUser: ERROR  in verifying user identity   "+e);
          });
    },
    //Function for verifying the property identity
    isProperty: function (req,res) {
             var propertyId = req.propertyId;
             //Validation
             if(!propertyId){
                          res({
                              "status_cd": 400,
                              "error" : {
                                     "error_cd" : 400,
                                     "message" : "Please enter a valid property Id"
                               }
                         });
                         return;
              }
              PropertyTransfer.deployed().then(function (instance) {
                             return instance.isProperty.call(propertyId);
              }).then(function (result) {
                            res({
                                "status_cd": 200,
                                "data" : {
                                    "message":result
                                 }
                            });
                             logger.info("isProperty:SUCCESS");
              }).catch(function (e) {
                           res({
                                "status_cd": 400,
                                     "error" : {
                                          "error_cd" : 400,
                                          "message" : "Error in verifying property identity"
                                      }
                           });
                          console.log(e);
                          logger.error("isProperty: ERROR  in verifying property identity   "+e);
            });
    },
    //Function for getting the property count of a user
    getUserPropertyCount: function (req,res) {
               var address = req.userAddress;
               //Validation
               if (!web3.isAddress(address)) {
                             res({
                                   "status_cd": 400,
                                   "error" : {
                                          "error_cd" : 400,
                                          "message" : "Please enter a valid address"
                                    }
                             });
                            return;
               }
              PropertyTransfer.deployed().then(function (instance) {
                          return instance.getUserPropertyCount.call(address);
              }).then(function (result) {
                          res({
                               "status_cd": 200,
                               "data" : {
                                        "count":result
                                }
                           });
                           logger.info("getUserPropertyCount:SUCCESS");
              }).catch(function (e) {
                          res({
                                    "status_cd": 400,
                                    "error" : {
                                              "error_cd" : 400,
                                              "message" : "Error in fetching the property count for user"
                                    }
                           });
                           console.log(e);
                           logger.error("getUserPropertyCount: ERROR in fetching the property count for user  "+e);
                        
              });
    },
    //Function for getting the property id of each user
    getUserPropertyId: function (req,res) {
                var address = req.userAddress;
                var id = req.id;
                //Validation
                if (!web3.isAddress(address)) {
                        res({
                                "status_cd": 400,
                                "error" : {
                                        "error_cd" : 400,
                                        "message" : "Please enter a valid address"
                                 }
                      
                        });
                        return;
                }
                if(!id){
                          res({
                                "status_cd": 400,
                                "error" : {
                                    "error_cd" : 400,
                                    "message" : "Please enter a valid index"
                                 }
                           });
                          return;
               }
               PropertyTransfer.deployed().then(function (instance) {
                            return instance.getUserPropertyAtIndex.call(address,id);
               }).then(function (result) {
                          if(result != 0){
                              res({
                                    "status_cd": 200,
                                    "data" : {
                                          "propertyId":result
                                     }
                                });
                                logger.info("getUserPropertyId:SUCCESS");
                          }else {
                               res({
                                    "status_cd": 200,
                                    "data" : {
                                          "propertyId":"Invalid Index"
                                     }
                                });
                                logger.info("getUserPropertyId:ERROR Invalid index");
                          }
                         
               }).catch(function (e) {
                         res({
                               "status_cd": 400,
                               "error" : {
                                     "error_cd" : 400,
                                     "message" : "Error in fetching the property id of user"
                                }
                         });
                         console.log(e);
                         logger.error("getUserPropertyId: ERROR in fetching the property id of user  "+e);
                         //console.log("Error in fetching the meterid of user");
             });
     },
     //Function for getting user's address
    getUsersAddress: function (req,res) {
      var id = req.id;
      //console.log(id);
      //Validation
      if(!id){
            res({
                 "status_cd": 400,
                  "error" : {
                         "error_cd" : 400,
                         "message" : "Please enter a valid index"
                  }
            });
         return;
      }
      PropertyTransfer.deployed().then(function (instance) {
                       return instance.getUserAddressAtIndex.call(id);
      }).then(function (result) {
                     //  console.log(result);
                     if(result != "0x"){
                        res({
                              "status_cd": 200,
                              "data" : {
                                     "userAddress":result
                               }
                         });
                        logger.info("getUsersAddress:SUCCESS");
                     }else {
                        res({
                              "status_cd": 200,
                              "data" : {
                                     "message":"Invalid index"
                               }
                         });
                        logger.error("getUsersAddress:ERROR Invalid index");
                     }
                    
       }).catch(function (e) {
                       res({
                                 "status_cd": 400,
                                 "error" : {
                                        "error_cd" : 400,
                                        "message" : "Error in fetching the user address"
                                  }
                        });
                      //console.log("Error in fetching the user address");
                      //console.log(e);
                      logger.error("getUsersAddress: ERROR-Error in fetching the user address  "+e);
         });
   },
  //Function for getting the user details based on address
   getUserDetails: function (req,res) {
            var userAddress = req.userAddress;
            //Validation
            if(!userAddress){
                         res({
                                "status_cd": 400,
                                "error" : {
                                      "error_cd" : 400,
                                      "message" : "Please enter a valid user id"
                                 }
                          });
                 return;
           }
           PropertyTransfer.deployed().then(function (instance) {
                       return instance.getUserDetailsByIndex.call(userAddress);
           }).then(function (result) {
                          //console.log(result);
                          // console.log(result[0].c[0]);
                          //  console.log(result[1].c[0]);
                          //  console.log(result[2].c[0]);
                          res({
                                  "status_cd": 200,
                                  "data" : {
                                       //"name":result[0].c[0],
                                      // "age":result[1].c[0],
                                      // "occupation":result[2].c[0],   
                                      result
                                  }       
                         });
                         logger.info("getUserDetails:SUCCESS");
           }).catch(function (e) {
                         res({
                                "status_cd": 400,
                                 "error" : {
                                           "error_cd" : 400,
                                           "message" : "Error in fetching the user details"
                                   }
                          });
                         console.log(e);
                         logger.error("getUserDetails: ERROR  in fetching the user details  "+e);
            });
    },

    //Function for getting the property details based on id
    getUserPropertyDetails: function (req,res) {
             var propertyId = req.propertyId;
             //Validation
             if(!propertyId){
                          res({
                                 "status_cd": 400,
                                 "error" : {
                                       "error_cd" : 400,
                                       "message" : "Please enter a valid property id"
                                  }
                           });
                  return;
            }
            PropertyTransfer.deployed().then(function (instance) {
                        return instance.getPropertyDetailsByIndex.call(propertyId);
            }).then(function (result) {
                           console.log(result);
                           // console.log(result[0].c[0]);
                           //  console.log(result[1].c[0]);
                           //  console.log(result[2].c[0]);
                           res({
                                   "status_cd": 200,
                                   "data" : {
                                    //     "sqfeet":result[0].c[0],
                                    //     "totalarea":result[1].c[0],
                                    result
                                        
                                   }       
                          });
                          logger.info("getUserPropertyDetails:SUCCESS");
            }).catch(function (e) {
                          res({
                                 "status_cd": 400,
                                  "error" : {
                                            "error_cd" : 400,
                                            "message" : "Error in fetching the property details"
                                    }
                           });
                          console.log(e);
                          logger.error("getUserPropertyDetails: ERROR  in fetching the property details  "+e);
             });
     },
     //Function to create user
    createUser: function (req,res) {
          var userAddress  = req.userAddress; 
          var name  = req.name;
          var age  = req.age;
          var occupation = req.occupation;
          var password = req.password;
          //new fields
          var emiratesId=req.emiratesId;
          var email=req.email;
          var contactNumber=req.contactNumber;
          if(!emiratesId){
            res({
                 "status_cd": 400,
                 "error" : {
                     "error_cd" : 400,
                     "message" : "Please enter a valid emiratesId"
                 }
            });
            return;
          }
          if(!email){
            res({
                 "status_cd": 400,
                 "error" : {
                     "error_cd" : 400,
                     "message" : "Please enter a valid email"
                 }
            });
            return;
          }
          if(!contactNumber){
            res({
                 "status_cd": 400,
                 "error" : {
                     "error_cd" : 400,
                     "message" : "Please enter a valid contact number"
                 }
            });
            return;
          }
          //Validation
          if(!name){
                res({
                     "status_cd": 400,
                     "error" : {
                         "error_cd" : 400,
                         "message" : "Please enter a valid name"
                     }
                });
                return;
          }
          if(isNaN(age)){
               res({
                    "status_cd": 400,
                    "error" : {
                         "error_cd" : 400,
                         "message" : "Please enter a valid age"
                     }
               });
               return;
          }
          if(!occupation){
            res({
                 "status_cd": 400,
                 "error" : {
                      "error_cd" : 400,
                      "message" : "Please enter a valid occupation"
                  }
            });
            return;
          }
          if(!password){
                 res({
                      "status_cd": 400,
                      "error" : {
                          "error_cd" : 400,
                          "message" : "Please enter a valid password"
                       }
                });
                 return;
          }
          if (!web3.isAddress(userAddress)) {
               res({
                   "status_cd": 400,
                   "error" : {
                          "error_cd" : 400,
                          "message" : "Please enter a valid address"
                   }
                
              });
             return;
          }

                //var contractInstance;
                web3.personal.unlockAccount(account,password,100,function(err,result){
                //console.log(result);
                if(result){

                         PropertyTransfer.deployed().then(function (instance) {
                                 // contractInstance = instance;
                                 return instance.createUser(userAddress,name,age,occupation,emiratesId,email,contactNumber,{from:account,gas:3000000});
                        }).then(function (result) {
                                      console.log(result);
                                      // console.log(result.tx);
                                      // console.log(result.logs[0].args.meterid.c[0]);
                                      var transactionHash = result.tx;
                                      if(result.logs.length>0) {
                                          res({
                                                "status_cd": 200,
                                                "data" : {
                                                       "transactionHash":transactionHash,
                                                }
                                        
                                          });  
                                         logger.info("createResident:SUCCESS");   
                                      }else {
                                          res({
                                                "status_cd": 200,
                                                "data" : {
                                                       "message":"Already registered user"
                                                }
                                        
                                          });  
                                         logger.error("createResident:ERROR Registered User");
                                      }
                                        
                        }).catch(function (e) {
                              res({
                                    "status_cd": 400,
                                    "error" : {
                                         "error_cd" : 400,
                                         "message" : "Error in creating resident"
                                     }
                               });
                               //console.log(e);
                              logger.error("createResident: ERROR in Creating resident  "+e);
                               //console.log("Error in Creating Meter Details");
                      });     


              }else {
                        res({
                                    "status_cd": 400,
                                    "error" : {
                                               "error_cd" : 400,
                                               "message" : "Error in Authentication"
                                     }
                        });
                       logger.error("createResident: ERROR in Authentication  "+err);
              }      
          });
},
//Function to create property
createProperty: function (req,res) {
      var propertyId=req.propertyId;
      var userAddress  = req.userAddress; 
      var sqfeet  = req.sqfeet;
      var totalArea  = req.totalArea;
      var password = req.password;
      //new fields
      var gpsCoordinates=req.gpsCoordinates;
      var landmark=req.landmark;
      if(!gpsCoordinates){
            res({
                 "status_cd": 400,
                 "error" : {
                     "error_cd" : 400,
                     "message" : "Please enter a valid gpsCoordinates"
                 }
            });
            return;
          }
          if(!landmark){
            res({
                 "status_cd": 400,
                 "error" : {
                     "error_cd" : 400,
                     "message" : "Please enter a valid landmark"
                 }
            });
            return;
          }
        
      //Validation
      if(isNaN(sqfeet)){
            res({
                 "status_cd": 400,
                 "error" : {
                     "error_cd" : 400,
                     "message" : "Please enter a valid sqfeet"
                 }
            });
            return;
      }
      if(isNaN(totalArea)){
           res({
                "status_cd": 400,
                "error" : {
                     "error_cd" : 400,
                     "message" : "Please enter a valid totalarea"
                 }
           });
           return;
      }
      if(isNaN(propertyId)){
        res({
             "status_cd": 400,
             "error" : {
                  "error_cd" : 400,
                  "message" : "Please enter a valid propertyid"
              }
        });
        return;
      }
      if(!password){
             res({
                  "status_cd": 400,
                  "error" : {
                      "error_cd" : 400,
                      "message" : "Please enter a valid password"
                   }
            });
             return;
      }
      if (!web3.isAddress(userAddress)) {
           res({
               "status_cd": 400,
               "error" : {
                      "error_cd" : 400,
                      "message" : "Please enter a valid address"
               }
            
          });
         return;
      }

            //var contractInstance;
            web3.personal.unlockAccount(account,password,100,function(err,result){
            //console.log(result);
            if(result){

                     PropertyTransfer.deployed().then(function (instance) {
                             // contractInstance = instance;
                             return instance.createProperty(propertyId,userAddress,sqfeet,totalArea,gpsCoordinates,landmark,{from:account,gas:3000000});
                     }).then(function (result) {
                                  console.log(result);
                                  // console.log(result.tx);
                                  // console.log(result.logs[0].args.meterid.c[0]);
                                  if(result.logs.length>0) {

                                      let ProCollection = new proCollection({
                                          address: userAddress,
                                          propertyId: propertyId
                                      });
                                      ProCollection.save((err, data) => {
                                          if (err) {
                                              res({
                                                  "status_cd": 400,
                                                  "error" : {
                                                      "error_cd" : 400,
                                                      "message" : "Error in creating property"
                                                  }
                                              });
                                          }

                                          res({
                                              "status_cd": 200,
                                              "data" : {
                                                  "transactionHash":result.tx,
                                              }

                                          });
                                          logger.info("createProperty:SUCCESS");

                                      });

                                }else {
                                    res({
                                          "status_cd": 200,
                                          "data" : {
                                                 "message":"Already Registered with propertyId"
                                          }
                                  
                                    });  
                                   logger.info("createProperty:ERROR Registered with PropertyId");
                                }     
                    }).catch(function (e) {
                          res({
                                "status_cd": 400,
                                "error" : {
                                     "error_cd" : 400,
                                     "message" : "Error in creating property"
                                 }
                           });
                           //console.log(e);
                          logger.error("createProperty: ERROR in Creating property  "+e);
                           //console.log("Error in Creating Meter Details");
                  });     


          }else {
                    res({
                                "status_cd": 400,
                                "error" : {
                                           "error_cd" : 400,
                                           "message" : "Error in Authentication"
                                 }
                    });
                   logger.error("createProperty: ERROR in Authentication  "+err);
          }      
      });
},   
   //Function to create user
   updateUser: function (req,res) {
      var userAddress  = req.userAddress; 
      var name  = req.name;
      var age  = req.age;
      var occupation = req.occupation;
      var password = req.password;
      //new fields
      var emiratesId=req.emiratesId;
      var email=req.email;
      var contactNumber=req.contactNumber;
      if(!emiratesId){
            res({
                 "status_cd": 400,
                 "error" : {
                     "error_cd" : 400,
                     "message" : "Please enter a valid emiratesId"
                 }
            });
            return;
          }
          if(!email){
            res({
                 "status_cd": 400,
                 "error" : {
                     "error_cd" : 400,
                     "message" : "Please enter a valid email"
                 }
            });
            return;
          }
          if(!contactNumber){
            res({
                 "status_cd": 400,
                 "error" : {
                     "error_cd" : 400,
                     "message" : "Please enter a valid contact number"
                 }
            });
            return;
          }
      //Validation
      if(!name){
            res({
                 "status_cd": 400,
                 "error" : {
                     "error_cd" : 400,
                     "message" : "Please enter a valid name"
                 }
            });
            return;
      }
      if(isNaN(age)){
           res({
                "status_cd": 400,
                "error" : {
                     "error_cd" : 400,
                     "message" : "Please enter a valid age"
                 }
           });
           return;
      }
      if(!occupation){
        res({
             "status_cd": 400,
             "error" : {
                  "error_cd" : 400,
                  "message" : "Please enter a valid occupation"
              }
        });
        return;
      }
      if(!password){
             res({
                  "status_cd": 400,
                  "error" : {
                      "error_cd" : 400,
                      "message" : "Please enter a valid password"
                   }
            });
             return;
      }
      if (!web3.isAddress(userAddress)) {
           res({
               "status_cd": 400,
               "error" : {
                      "error_cd" : 400,
                      "message" : "Please enter a valid address"
               }
            
          });
         return;
      }

            //var contractInstance;
            web3.personal.unlockAccount(account,password,100,function(err,result){
            //console.log(result);
            if(result){

                     PropertyTransfer.deployed().then(function (instance) {
                             // contractInstance = instance;
                             return instance.updateUser(userAddress,name,age,occupation,emiratesId,email,contactNumber,{from:account,gas:3000000});
                    }).then(function (result) {
                                  console.log(result);
                                  // console.log(result.tx);
                                  // console.log(result.logs[0].args.meterid.c[0]);
                                  var transactionHash = result.tx;
                                  if(result.logs.length>0) {
                                       res({
                                             "status_cd": 200,
                                             "data" : {
                                                 "transactionHash":result.tx,
                                             }
                                  
                                        });  
                                       logger.info("updateProperty:SUCCESS");   
                                  }else {
                                        res({
                                              "status_cd": 200,
                                              "data" : {
                                                    "message":"Invalid data"
                                              }
                                  
                                        });  
                                       logger.info("createProperty:ERROR Invalid data");
                                }         
                    }).catch(function (e) {
                          res({
                                "status_cd": 400,
                                "error" : {
                                     "error_cd" : 400,
                                     "message" : "Error in updating resident"
                                 }
                           });
                           //console.log(e);
                          logger.error("updateUser: ERROR in updating resident  "+e);
                           //console.log("Error in Creating Meter Details");
                  });     


          }else {
                    res({
                                "status_cd": 400,
                                "error" : {
                                           "error_cd" : 400,
                                           "message" : "Error in Authentication"
                                 }
                    });
                   logger.error("updateUser: ERROR in Authentication  "+err);
          }      
      });
},
//Function to create property
updateProperty: function (req,res) {
  var propertyId=req.propertyId;
  var userAddress  = req.userAddress; 
  var sqfeet  = req.sqfeet;
  var totalArea  = req.totalArea;
  var password = req.password;
   //new fields
   var gpsCoordinates=req.gpsCoordinates;
   var landmark=req.landmark;
   if(!gpsCoordinates){
      res({
           "status_cd": 400,
           "error" : {
               "error_cd" : 400,
               "message" : "Please enter a valid gpsCoordinates"
           }
      });
      return;
    }
    if(!landmark){
      res({
           "status_cd": 400,
           "error" : {
               "error_cd" : 400,
               "message" : "Please enter a valid landmark"
           }
      });
      return;
    }
  //Validation
  if(isNaN(sqfeet)){
        res({
             "status_cd": 400,
             "error" : {
                 "error_cd" : 400,
                 "message" : "Please enter a valid sqfeet"
             }
        });
        return;
  }
  if(isNaN(totalArea)){
       res({
            "status_cd": 400,
            "error" : {
                 "error_cd" : 400,
                 "message" : "Please enter a valid totalarea"
             }
       });
       return;
  }
  if(isNaN(propertyId)){
    res({
         "status_cd": 400,
         "error" : {
              "error_cd" : 400,
              "message" : "Please enter a valid propertyid"
          }
    });
    return;
  }
  if(!password){
         res({
              "status_cd": 400,
              "error" : {
                  "error_cd" : 400,
                  "message" : "Please enter a valid password"
               }
        });
         return;
  }
  if (!web3.isAddress(userAddress)) {
       res({
           "status_cd": 400,
           "error" : {
                  "error_cd" : 400,
                  "message" : "Please enter a valid address"
           }
        
      });
     return;
  }

        //var contractInstance;
        web3.personal.unlockAccount(account,password,100,function(err,result){
        //console.log(result);
        if(result){

                 PropertyTransfer.deployed().then(function (instance) {
                         // contractInstance = instance;
                         return instance.updateProperty(propertyId,userAddress,sqfeet,totalArea,gpsCoordinates,landmark,{from:account,gas:3000000});
                 }).then(function (result) {
                              console.log(result);
                              // console.log(result.tx);
                              // console.log(result.logs[0].args.meterid.c[0]);
                              var transactionHash = result.tx;
                              if(result.logs.length>0) {
                                    res({
                                          "status_cd": 200,
                                          "data" : {
                                                 "transactionHash":result.tx,
                                          }
                                  
                                    });  
                                   logger.info("updateProperty:SUCCESS");   
                                }else {
                                    res({
                                          "status_cd": 200,
                                          "data" : {
                                                 "message":"Invalid data"
                                          }
                                  
                                    });  
                                   logger.info("updateProperty:ERROR Invalid data");
                                }       
                }).catch(function (e) {
                      res({
                            "status_cd": 400,
                            "error" : {
                                 "error_cd" : 400,
                                 "message" : "Error in updating property"
                             }
                       });
                       //console.log(e);
                      logger.error("updateProperty: ERROR in updating property  "+e);
                       //console.log("Error in Creating Meter Details");
              });     


      }else {
                res({
                            "status_cd": 400,
                            "error" : {
                                       "error_cd" : 400,
                                       "message" : "Error in Authentication"
                             }
                });
               logger.error("updateProperty: ERROR in Authentication  "+err);
      }      
  });
},
 //Function for getting property
 getPropertyAtIndex: function (req,res) {
      var id = req.id;
      //console.log(id);
      //Validation
      if(!id){
            res({
                 "status_cd": 400,
                  "error" : {
                         "error_cd" : 400,
                         "message" : "Please enter a valid index"
                  }
            });
         return;
      }
      PropertyTransfer.deployed().then(function (instance) {
                       return instance.getPropertyAtIndex.call(id);
      }).then(function (result) {
                     
                        res({
                              "status_cd": 200,
                              "data" : {
                                     "propertyId":result
                               }
                         });
                        logger.info("getPropertyAtIndex:SUCCESS");
                    
                    
       }).catch(function (e) {
                       res({
                                 "status_cd": 400,
                                 "error" : {
                                        "error_cd" : 400,
                                        "message" : "Error in fetching the property"
                                  }
                        });
                      //console.log("Error in fetching the user address");
                      //console.log(e);
                      logger.error("getPropertyAtIndex: ERROR-Error in fetching the property  "+e);
         });
 },

//Latest Functions
//Function to buy request
buyRequest: function (req,res) {
      var propertyId=req.propertyId;
      var userAddress  = req.currentUserAddress; 
      var newOwnerAddress  = req.newOwnerAddress; 
      var password = req.password;
      //Validation
    
      if(isNaN(propertyId)){
        res({
             "status_cd": 400,
             "error" : {
                  "error_cd" : 400,
                  "message" : "Please enter a valid propertyid"
              }
        });
        return;
      }
      if(!password){
             res({
                  "status_cd": 400,
                  "error" : {
                      "error_cd" : 400,
                      "message" : "Please enter a valid password"
                   }
            });
             return;
      }
      if (!web3.isAddress(userAddress)) {
           res({
               "status_cd": 400,
               "error" : {
                      "error_cd" : 400,
                      "message" : "Please enter a valid address"
               }
            
          });
         return;
      }
      if (!web3.isAddress(newOwnerAddress)) {
            res({
                "status_cd": 400,
                "error" : {
                       "error_cd" : 400,
                       "message" : "Please enter a new owner address"
                }
             
           });
          return;
       }
    
            //var contractInstance;
            web3.personal.unlockAccount(account,password,100,function(err,result){
            //console.log(result);
            if(result){
    
                     PropertyTransfer.deployed().then(function (instance) {
                             // contractInstance = instance;
                             return instance.buyRequest(userAddress,propertyId,newOwnerAddress,{from:account,gas:3000000});
                     }).then(function (result) {
                                  console.log(result);
                                  // console.log(result.tx);
                                  // console.log(result.logs[0].args.meterid.c[0]);
                                  var transactionHash = result.tx;
                             
                                        res({
                                              "status_cd": 200,
                                              "data" : {
                                                     "transactionHash":result.tx,
                                              }
                                      
                                        });  
                                       logger.info("buyRequest:SUCCESS");   
                                      
                    }).catch(function (e) {
                          res({
                                "status_cd": 400,
                                "error" : {
                                     "error_cd" : 400,
                                     "message" : "Error in adding"
                                 }
                           });
                           //console.log(e);
                          logger.error("buyRequest: ERROR in adding buyRequest  "+e);
                           //console.log("Error in Creating Meter Details");
                  });     
    
    
          }else {
                    res({
                                "status_cd": 400,
                                "error" : {
                                           "error_cd" : 400,
                                           "message" : "Error in Authentication"
                                 }
                    });
                   logger.error("buyRequest: ERROR in Authentication  "+err);
          }      
      });
    },

    //Function to buy request
buyRequestCheck: function (req,res) {
     
      var userAddress  = req.userAddress; 
    
      if (!web3.isAddress(userAddress)) {
           res({
               "status_cd": 400,
               "error" : {
                      "error_cd" : 400,
                      "message" : "Please enter a valid address"
               }
            
          });
         return;
      }
     
    
           
    
                     PropertyTransfer.deployed().then(function (instance) {
                             // contractInstance = instance;
                             return instance.buyRequestCheck.call(userAddress);
                     }).then(function (result) {
                                  console.log(result);
                              
                                        res({
                                              "status_cd": 200,
                                              "data" : {
                                                     result,
                                              }
                                      
                                        });  
                                       logger.info("buyRequestCheck:SUCCESS");   
                                       
                    }).catch(function (e) {
                          res({
                                "status_cd": 400,
                                "error" : {
                                     "error_cd" : 400,
                                     "message" : "Error in fetching buyrequestcheck"
                                 }
                           });
                           //console.log(e);
                          logger.error("buyRequestCheck: ERROR in fetching buyrequestcheck  "+e);
                           //console.log("Error in Creating Meter Details");
                  });     
    
    
       
 },
 //Function for getting count of request
 getRequestCount: function(res) {
      PropertyTransfer.deployed().then(function (instance) {
                  return instance.getRequestCount.call();
      }).then(function (result) {
              res({
                    "status_cd": 200,
                    "data" : {
                            "count":result
                    }
               });
               logger.info("getRequestCount:SUCCESS");
      }).catch(function (e) {
               res({
                       "status_cd": 400,
                       "error" : {
                             "error_cd" : 400,
                             "message" : "Error in fetching the request count"
                       }
                });
                console.log(e);
                logger.error("getRequestCount: ERROR-Error in fetching the request count  "+e);
                //console.log("Error in fetching the user count");
    });
},
  //Function for getting requested user's address
  getRequestAddressAtIndex: function (req,res) {
      var id = req.id;
      //console.log(id);
      //Validation
      if(!id){
            res({
                 "status_cd": 400,
                  "error" : {
                         "error_cd" : 400,
                         "message" : "Please enter a valid index"
                  }
            });
         return;
      }
      PropertyTransfer.deployed().then(function (instance) {
                       return instance.getRequestAddressAtIndex.call(id);
      }).then(function (result) {
                     //  console.log(result);
                     if(result != "0x"){
                        res({
                              "status_cd": 200,
                              "data" : {
                                     "userAddress":result
                               }
                         });
                        logger.info("getRequestAddressAtIndex:SUCCESS");
                     }else {
                        res({
                              "status_cd": 200,
                              "data" : {
                                     "message":"Invalid index"
                               }
                         });
                        logger.error("getRequestAddressAtIndex:ERROR Invalid index");
                     }
                    
       }).catch(function (e) {
                       res({
                                 "status_cd": 400,
                                 "error" : {
                                        "error_cd" : 400,
                                        "message" : "Error in fetching the requested user address"
                                  }
                        });
                      //console.log("Error in fetching the user address");
                      //console.log(e);
                      logger.error("getRequestAddressAtIndex: ERROR-Error in fetching the requested user address  "+e);
         });
 },
 
//Function to buy request update
buyRequestUpdate: function (req,res) {
      var userAddress  = req.userAddress; 
      var password = req.password;
      //Validation
    
      if(!password){
             res({
                  "status_cd": 400,
                  "error" : {
                      "error_cd" : 400,
                      "message" : "Please enter a valid password"
                   }
            });
             return;
      }
      if (!web3.isAddress(userAddress)) {
           res({
               "status_cd": 400,
               "error" : {
                      "error_cd" : 400,
                      "message" : "Please enter a valid address"
               }
            
          });
         return;
      }
     
    
            //var contractInstance;
            web3.personal.unlockAccount(account,password,100,function(err,result){
            //console.log(result);
            if(result){
    
                     PropertyTransfer.deployed().then(function (instance) {
                             // contractInstance = instance;
                             return instance.buyRequestUpdate(userAddress,{from:account,gas:3000000});
                     }).then(function (result) {
                                  console.log(result);
                                  // console.log(result.tx);
                                  // console.log(result.logs[0].args.meterid.c[0]);
                                  var transactionHash = result.tx;
                             
                                        res({
                                              "status_cd": 200,
                                              "data" : {
                                                     "transactionHash":result.tx,
                                              }
                                      
                                        });  
                                       logger.info("buyRequest:SUCCESS");   
                                    
                    }).catch(function (e) {
                          res({
                                "status_cd": 400,
                                "error" : {
                                     "error_cd" : 400,
                                     "message" : "Error in adding confirmation in buyRequest by user"
                                 }
                           });
                           //console.log(e);
                          logger.error("buyRequest: ERROR in adding confirmation in buyRequest by user  "+e);
                           //console.log("Error in Creating Meter Details");
                  });     
    
    
          }else {
                    res({
                                "status_cd": 400,
                                "error" : {
                                           "error_cd" : 400,
                                           "message" : "Error in Authentication"
                                 }
                    });
                   logger.error("buyRequest: ERROR in Authentication  "+err);
          }      
      });
 },
 //Function to buy request confirm by admin
 buyRequestConfirm: function (req,res) {
     
      var userAddress  = req.userAddress; 
     
      //Validation
    
      
      if (!web3.isAddress(userAddress)) {
           res({
               "status_cd": 400,
               "error" : {
                      "error_cd" : 400,
                      "message" : "Please enter a valid address"
               }
            
          });
         return;
      }
     
    
           
    
                     PropertyTransfer.deployed().then(function (instance) {
                             // contractInstance = instance;
                             return instance.buyRequestConfirm.call(userAddress);
                     }).then(function (result) {
                                  console.log(result);
                                  
                                        res({
                                              "status_cd": 200,
                                              "data" : {
                                                     result,
                                              }
                                      
                                        });  
                                       logger.info("buyRequestConfirm:SUCCESS");   
                                    
                    }).catch(function (e) {
                          res({
                                "status_cd": 400,
                                "error" : {
                                     "error_cd" : 400,
                                     "message" : "Error in fetching buyrequestconfirm by admin"
                                 }
                           });
                           //console.log(e);
                          logger.error("buyRequestConfirm: ERROR in fetching buyrequestconfirm by admin "+e);
                           //console.log("Error in Creating Meter Details");
                  });     
    
    
         
 },
//update
//Function to update
update: function (req,res) {
      var propertyId=req.propertyId;
      var newOwnerAddress  = req.newOwnerAddress; 
      var password = req.password;
      //Validation
    
      if(isNaN(propertyId)){
        res({
             "status_cd": 400,
             "error" : {
                  "error_cd" : 400,
                  "message" : "Please enter a valid propertyid"
              }
        });
        return;
      }
      if(!password){
             res({
                  "status_cd": 400,
                  "error" : {
                      "error_cd" : 400,
                      "message" : "Please enter a valid password"
                   }
            });
             return;
      }
      if (!web3.isAddress(newOwnerAddress)) {
            res({
                "status_cd": 400,
                "error" : {
                       "error_cd" : 400,
                       "message" : "Please enter a new owner address"
                }
             
           });
          return;
       }
    
            //var contractInstance;
            web3.personal.unlockAccount(account,password,100,function(err,result){
            //console.log(result);
            if(result){
    
                     PropertyTransfer.deployed().then(function (instance) {
                             // contractInstance = instance;
                             return instance.update(newOwnerAddress,propertyId,{from:account,gas:3000000});
                     }).then(function (result) {
                                  console.log(result);
                                  // console.log(result.tx);
                                  // console.log(result.logs[0].args.meterid.c[0]);
                                  var transactionHash = result.tx;
                                        res({
                                              "status_cd": 200,
                                              "data" : {
                                                     "transactionHash":result.tx,
                                              }
                                      
                                        });  
                                       logger.info("update:SUCCESS");     
                    }).catch(function (e) {
                          res({
                                "status_cd": 400,
                                "error" : {
                                     "error_cd" : 400,
                                     "message" : "Error in transfer by admin"
                                 }
                           });
                           //console.log(e);
                          logger.error("update: ERROR in transfer by admin  "+e);
                           //console.log("Error in Creating Meter Details");
                  });     
    
    
          }else {
                    res({
                                "status_cd": 400,
                                "error" : {
                                           "error_cd" : 400,
                                           "message" : "Error in Authentication"
                                 }
                    });
                   logger.error("buyRequest: ERROR in Authentication  "+err);
          }      
      });
 },

//deleting requested API
//Function to update
deleteRequestedUser: function (req,res) {
      //var propertyId=req.propertyId;
     // var newOwnerAddress  = req.newOwnerAddress; 
      var userAddress= req.userAddress;
      var password = req.password;
      //Validation
    
     
      if(!password){
             res({
                  "status_cd": 400,
                  "error" : {
                      "error_cd" : 400,
                      "message" : "Please enter a valid password"
                   }
            });
             return;
      }
      if (!web3.isAddress(userAddress)) {
            res({
                "status_cd": 400,
                "error" : {
                       "error_cd" : 400,
                       "message" : "Please enter a new user address"
                }
             
           });
          return;
       }
    
            //var contractInstance;
            web3.personal.unlockAccount(account,password,100,function(err,result){
            //console.log(result);
            if(result){
    
                     PropertyTransfer.deployed().then(function (instance) {
                             // contractInstance = instance;
                             return instance.deleteRequestedUser(userAddress,{from:account,gas:3000000});
                     }).then(function (result) {
                                  console.log(result);
                                  // console.log(result.tx);
                                  // console.log(result.logs[0].args.meterid.c[0]);
                                  var transactionHash = result.tx;
                                        res({
                                              "status_cd": 200,
                                              "data" : {
                                                     "transactionHash":result.tx,
                                              }
                                      
                                        });  
                                       logger.info("update:SUCCESS");     
                    }).catch(function (e) {
                          res({
                                "status_cd": 400,
                                "error" : {
                                     "error_cd" : 400,
                                     "message" : "Error in deleting the requested user"
                                 }
                           });
                           //console.log(e);
                          logger.error("update: ERROR in deleting the requested user  "+e);
                           //console.log("Error in Creating Meter Details");
                  });     
    
    
          }else {
                    res({
                                "status_cd": 400,
                                "error" : {
                                           "error_cd" : 400,
                                           "message" : "Error in Authentication"
                                 }
                    });
                   logger.error("buyRequest: ERROR in Authentication  "+err);
          }      
      });
 }


};
module.exports = App;
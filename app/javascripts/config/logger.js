var log4js = require('log4js');

   log4js.configure({
         appenders: {
                   consoleAppender: { type: 'console' },
                   fileAppenderCreateUser: { type: 'file', filename: 'LOGS/CreateUser/logs.log' },
                   fileAppenderEnergy: { type: 'file', filename: 'LOGS/Property/logs.log' },
         },
         categories: {
                  default: { appenders: ['consoleAppender', 'fileAppenderCreateUser'], level: 'All' },
                  defaultCreateUser: { appenders: ['consoleAppender', 'fileAppenderCreateUser'], level: 'All' },
                  defaultProperty: { appenders: ['consoleAppender', 'fileAppenderEnergy'], level: 'All' } 
        }
   });

    var loggerCreateUser = log4js.getLogger('defaultCreateUser');
    var loggerProperty = log4js.getLogger('defaultProperty');

    var getLoggerCreateUser = function() {
        return loggerCreateUser;
    };
    var getLoggerProperty = function() {
        return loggerProperty;
    };
   
exports.loggerCreateUser = getLoggerCreateUser();
exports.loggerProperty = getLoggerProperty();

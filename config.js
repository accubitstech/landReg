module.exports = {
  development: {
    host: "localhost", 
    port: 22000
  },
    collections: {
        accountCollection: 'account_collections',
        propertyCollection: 'property_collections',
        bootNodeCollection: 'boot_collections'
    }
};
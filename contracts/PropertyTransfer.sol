pragma solidity ^0.4.18;

contract PropertyTransfer {

    address public owner;
    //UserStruct
    struct UserStruct {
           uint userListPointer; 
           uint[] propertyKeys; 
           mapping(uint => uint) propertyKeyPointers;

           string name;
           uint age;
           string occupation;
           //new fields
           uint emiratesId;
           string email;
           uint contactNumber;
    }
    mapping(address => UserStruct) public userStructs;
    address[] public userList;
    //MeterStruct
    struct PropertyStruct {
            uint propertyListPointer; 
            address userKey;
            uint256  sqfeet;
            uint256 totalArea;
            //new fields
            string gpsCoordinates;
            string landmark;

    }
    mapping(uint => PropertyStruct) public propertyStructs;
    uint[] public propertyList;

    event LogCreateUser(address userAddress);
    event LogCreateProperty(uint propertyId);
    event LogUpdateUser(address userAddress);
    event LogUpdateProperty(uint propertyId);
    event LogUpdateTransfer(uint propertyId);

    function PropertyTransfer() public {
        owner = msg.sender;
    }

   //***** */
    struct RequestStruct {
            uint propertyId; 
            address newOwnerAddress;
            address currentOwnerAddress;
            uint requestCount;
            uint index;
    }
    mapping(address => RequestStruct) public requestStructs;
    address[] public addressList;
   //**** */


    //Function to get the count of users
    function getUserCount() public constant returns(uint userCount) {
         return userList.length;
    }
    //Function to get the count of property
    function getPropertyCount() public constant returns(uint propertyCount) {
        return propertyList.length;
    }
    //Function to verify the address is existing or not
    function isUser(address userId) public constant returns(bool isIndeed) {
          if (userList.length==0) 
              return false;
          return userList[userStructs[userId].userListPointer]==userId;
    }
    //Function to verify the propertyid is existing or not
    function isProperty(uint propertyId) public constant  returns(bool isIndeed) {
          if (propertyList.length==0) 
             return false;
          return propertyList[propertyStructs[propertyId].propertyListPointer]==propertyId;
    }

    //Function to get the user address
    function getUserAddressAtIndex(uint8 row)  public  constant  returns(address userAddress) {
          return userList[row];
    }
    //Function to get the property id
    function getPropertyAtIndex(uint8 row)  public  constant  returns(uint propertyId) {
          return propertyList[row];
    }
    //Function to get the count of properties for specified user address
    function getUserPropertyCount(address userId)  public  constant  returns(uint256 propertyCount) {
          if (!isUser(userId)) 
              revert();
          return userStructs[userId].propertyKeys.length;
    }
    //Function to get the property ids for specified user address
    function getUserPropertyAtIndex(address userId, uint8 row)  public  constant   returns(uint256 propertyKey) {
            if (!isUser(userId)) 
               revert();
            return userStructs[userId].propertyKeys[row];
    }
    //Function to get the user details based on address
    function getUserDetailsByIndex(address userId) public constant returns(string name,uint age,string occupation,uint emiratesId,string email,uint contactNumber) {
             if (!isUser(userId)) 
                revert();
              return (userStructs[userId].name,
                   userStructs[userId].age,
                   userStructs[userId].occupation,
                   userStructs[userId].emiratesId,
                   userStructs[userId].email,
                   userStructs[userId].contactNumber
                   );
    }
    //Function to get the property details based on id
    function getPropertyDetailsByIndex(uint propertyId) public constant returns(uint256 sqfeet,uint256 totalArea,string gpsCoordinates,string landmark,address user) {
            if (!isProperty(propertyId)) 
                revert();
           return (propertyStructs[propertyId].sqfeet,
                   propertyStructs[propertyId].totalArea,
                   propertyStructs[propertyId].gpsCoordinates,
                   propertyStructs[propertyId].landmark,
                   propertyStructs[propertyId].userKey
                   );
    }
    //Function to create User
    function createUser(address userId,string name,uint age,string occupation,uint emiratesId,string email,uint contactNumber) public returns(bool isIndeed) {
          if (isUser(userId)) 
             revert();
          userStructs[userId].name = name;
          userStructs[userId].age = age;
          userStructs[userId].occupation = occupation;
          //new fields
          userStructs[userId].emiratesId = emiratesId;
          userStructs[userId].email = email;
          userStructs[userId].contactNumber = contactNumber;
          //new fields
          userStructs[userId].userListPointer = userList.push(userId)-1;
          LogCreateUser(userId);
          return true;
    } 
    //Function to create property for each user
    function createProperty(uint propertyId,address userId,uint256 sqfeet,uint256 totalArea,string gpsCoordinates,string landmark) public returns(bool isIndeed){
            if (!isUser(userId)) 
                revert();
            if (isProperty(propertyId)) 
                revert();
            propertyStructs[propertyId].propertyListPointer = propertyList.push(propertyId)-1;
            propertyStructs[propertyId].userKey = userId; 
            //Assign Values
            propertyStructs[propertyId].sqfeet = sqfeet;
            propertyStructs[propertyId].totalArea = totalArea;
            //new fields
            propertyStructs[propertyId].gpsCoordinates = gpsCoordinates;
            propertyStructs[propertyId].landmark = landmark;

           
            userStructs[userId].propertyKeyPointers[propertyId] = userStructs[userId].propertyKeys.push(propertyId) - 1;
            LogCreateProperty(propertyId);
            return true;
    }  
    //Function to update user
    function updateUser(address userId,string name,uint age,string occupation,uint emiratesId,string email,uint contactNumber)  public
         returns(bool success) 
        {
           if (!isUser(userId)) 
               revert(); 
           userStructs[userId].name = name;
           userStructs[userId].age = age;
           userStructs[userId].occupation = occupation;
           //new fields
           userStructs[userId].emiratesId = emiratesId;
           userStructs[userId].email = email;
           userStructs[userId].contactNumber = contactNumber;
           LogUpdateUser(userId);
           return true;
  }
  //Function to update property
   function updateProperty(uint propertyId,address userId,uint256 sqfeet,uint256 totalArea,string gpsCoordinates,string landmark)  public
         returns(bool success) 
        {
            if (!isUser(userId)) 
                revert();
           if (!isProperty(propertyId)) 
               revert(); 
           propertyStructs[propertyId].sqfeet = sqfeet;
           propertyStructs[propertyId].totalArea = totalArea;
           //new fields
           propertyStructs[propertyId].gpsCoordinates = gpsCoordinates;
           propertyStructs[propertyId].landmark = landmark;
           LogUpdateProperty(propertyId);
           return true;
  }

  //Functions buyRequest 
  function buyRequest(address userAddress,uint propertyId,address newOwnerAddress) public returns(bool success) {
      if (!isUser(userAddress)) {
               revert();
      }
       
      if (!isProperty(propertyId)) {
          revert(); 
      }
               
      //address newOwner = msg.sender; -fetch from from address later
      address newOwner = newOwnerAddress;
      // Required
      //   if (!isUser(newOwner)) {
      //       revert();
      //   }     
      requestStructs[userAddress].propertyId = propertyId;
      requestStructs[userAddress].newOwnerAddress = newOwner;
      requestStructs[userAddress].requestCount = 1;
      requestStructs[userAddress].currentOwnerAddress = userAddress;
      //setting index for delete
      requestStructs[userAddress].index = addressList.push(userAddress)-1;
     // addressList.push(userAddress);
      return true;
  }
  //Functions buyRequest at user side
  function buyRequestCheck(address userAddressInput) public view returns(address user,uint propertyId) {
          // address userAddress = msg.sender;
          address userAddress = userAddressInput;
          if (!isUser(userAddress)) 
                       revert();
          if (requestStructs[userAddress].requestCount == 1) {
                  return (requestStructs[userAddress].newOwnerAddress,
                  requestStructs[userAddress].propertyId);
          } 
  }
  //Function to get the count of users
  function getRequestCount() public constant returns(uint userCount) {
         return addressList.length;
   }
   //Function to get the user  request address
   function getRequestAddressAtIndex(uint8 row)  public  constant  returns(address userAddress) {
          return addressList[row];
   }
  //Functions to update request count
  function buyRequestUpdate(address userAddressInput) public  returns(bool success) {
          //address userAddress = msg.sender;
          address userAddress = userAddressInput;
          requestStructs[userAddress].requestCount = 2;
          return true;
  }
  //Functions to confirm buy request 
  function buyRequestConfirm(address user) public returns(address newowner,uint propertyid) {
          if (requestStructs[user].requestCount == 2) {
                 address newOwner = requestStructs[user].newOwnerAddress;
                 uint propertyId = requestStructs[user].propertyId;
                  return (newOwner,propertyId);
          }
          
  }
  //deletion
  function update(address newOwner,uint propertyId) public returns(bool success) {
        if (!isProperty(propertyId)) {
            revert();
        }
      //delete from user list
      address userId = propertyStructs[propertyId].userKey; 
      address verify = requestStructs[userId].currentOwnerAddress;
      if (userId == verify ) {
             if (requestStructs[userId].requestCount == 2) {
                  uint rowToDelete = userStructs[userId].propertyKeyPointers[propertyId];
                  uint keyToMove = userStructs[userId].propertyKeys[userStructs[userId].propertyKeys.length-1];
                  userStructs[userId].propertyKeys[rowToDelete] = keyToMove;
                  userStructs[userId].propertyKeyPointers[keyToMove] = rowToDelete;
                  userStructs[userId].propertyKeys.length--;
                  propertyStructs[propertyId].userKey = newOwner; 
                  userStructs[newOwner].propertyKeyPointers[propertyId] = userStructs[newOwner].propertyKeys.push(propertyId) - 1;
                  LogUpdateTransfer(propertyId);
                  return true;
             }{
                 revert();
             }
      } else {
          revert();
      }
     
    }
    //
     function deleteRequestedUser(address userAddress)  public returns(uint index) {
 
            uint rowToDelete = requestStructs[userAddress].index;
            address keyToMove = addressList[addressList.length-1];
            addressList[rowToDelete] = keyToMove;
            requestStructs[keyToMove].index = rowToDelete; 
            addressList.length--;
            return rowToDelete;
  }
}
# Prerequisites
   1. Ubuntu machine 16.04.3 LTS 
   2. nodejs 8+
   3. zip file containing four script files (quorum.sh,child_node.sh , add_peer.sh, app.sh)
 

# Steps
  1. adding master node
    * Unzip the file this will have scripts file required to setup masternodes,child nodes as well to make the child and master peer 2 peer respectively 
    * Go the new instance where you want your quorum master node1 to be installed check all prerequisites.we will be using quorum script to run master node from zip file take quorum. sh run ```(sudo) ./quorum.sh -m 1 <ip address of db server>```
    * On running the script you will be prompted to enter a password leave that blank and press enter you will be asked to setup master node at this point leave the server as it is
    * Go the new instance where you want your quorum master node1 to be installed check all prerequisites.we will be using quorum script to run master node from zip file take quorum. sh run ```(sudo) ./quorum.sh -m 2 <ip address of db server>```
    * On running the script you will be prompted to enter a password leave that blank and press enter go back to instance 1 and enter y to continue
    * By this time your master nodes will be connected and you can check this by typing ```(sudo) geth attach quorum/qdata/geth.ipc ``` this will open the console type ```admin.peers`` check if other ip address is listed there
  2. smart contract initialisation
    * Go to the project folder(landReg), then Run the command ```truffle compile``` and ```truffle migrate --reset```
  3. Setting up child node
     * Go the new instance where you want your quorum child node to be installed, check all prerequisites we will be using quorum script to run child node from zip file take quorum. sh run ```(sudo) ./child_node.sh -c ```
     * This will prompt for a raft id from master node to join to the network and will display its enode value copy that value with quotes ignoring square bracket([])
     * Go to any of the master node run the script ```sudo ./add_peer.sh <'enode value copied from child node with this single quotes'>```
     * for example your command would look something like ```sudo ./add_peer.sh '"enode://1f4df6bc3b46ce94387f2679e5698906e7f710f8a52684bca8e20c4fea94d165a6de477c3ddb0bbe7dc23dad40ea4393fb23e6f58cbcbbd755b1c915ce0b144a@54.255.224.99:21000?discport=0&raftport=23000"'```
 Note that there are two quotes in putting this is important 
     * Running this script would return a a number take this number and go to childnode instance and enter it there press enter your child node would be up and running connected to master nodes
     * repeat setting up of child node step in any number of instance you want.



Documentation of APIs to communicate with Blockchain network


# To Create User

> Endpoint     : api/v1/createUser

> Request type : POST

| Key                    | Value          | Comment          |
| :-----------           | :------:       | :------:         | 
|password |12345|                  |

### Responses
#### Success


  `{
          "status_cd": 200,
          "data": {
                "address": "0x624379bbfcad0cb366034d2940ad5bf143bb5102",
                "key": "eyJhZGRyZXNzIjoiNjI0Mzc5YmJmY2FkMGNiMzY2MDM0ZDI5NDBhZDViZjE0M2JiNTEwMiIsImNyeXB0byI6eyJjaXBoZXIiOiJhZXMtMTI4LWN0ciIsImNpcGhlcnRleHQiOiJiYjIxOTcxODg5NjRjYmZiZDQyYTRmN2U4NzljM2M3ZWI2MGFlOGNhMzY0NTQwMTNkM2Y1MWNlNzY1OWUxZjk5IiwiY2lwaGVycGFyYW1zIjp7Iml2IjoiYjMwMTUxY2VhNzA4MWVkZmE5YTkwZTM2Y2ZjMjU3MWQifSwibWFjIjoiMDhkZmRiNjg3MTBmZDNmM2U4YmMxMTA5YmZjYmUyMmYyNWJjNTEzZWRlOTY5MDQxZjljMjE3MDEwYjJiMjIwMSIsImtkZiI6InBia2RmMiIsImtkZnBhcmFtcyI6eyJjIjoyNjIxNDQsImRrbGVuIjozMiwicHJmIjoiaG1hYy1zaGEyNTYiLCJzYWx0IjoiYTg4MWU2NjM5NzdjOTYzZTllYTNmMzk1OWViZjUxYzUzMTUwY2IzYzBkMTM4M2NiOWVmZjdkYjdhYjE1OTRkYyJ9fSwiaWQiOiJkMmY5MGI0ZC1lZDc4LTQ0YTctYjlhYy0zOGQzMDFkMjkwOWUiLCJ2ZXJzaW9uIjozfQ=="
    }
  }`

#### Fail
`{
           "status_cd": 400,
           "error" : {
                    "error_cd" : 400,
                    "message" : "Error in creating address"
           }
}`
### Description
          To create a new user.

# To get total user/residents count

> Endpoint     : api/v1/getUserCount

> Request type : GET


### Responses
#### Success



  `{
         "status_cd": 200,
          "data" : {
                   "count":2
           }
  }`

#### Fail
`{
        "status_cd": 400,
         "error" : {
                       "error_cd" : 400,
                        "message" : "Error in fetching the user count"
          }
}`
### Description
          To get the user total count


# To get total property count

> Endpoint     : api/v1/getPropertyCount

> Request type : GET


### Responses
#### Success


  `{
         "status_cd": 200,
          "data" : {
                "count":2
           }
  }`

#### Fail 
`{
       "status_cd": 400,
        "error" : {
                  "error_cd" : 400,
                  "message" : "Error in fetching the property count"
         }
}`
### Description
          To get the total property count

# To verify whether the user is registered or not

> Endpoint     : api/v1/isUser

> Request type : POST

| Key                    | Value          | Comment          |
| :-----------           | :------:       | :------:         | 
|userAddress|0x549b6c3ba891530bba3856b2adb283c9c4045d64|

### Responses
#### Success

 `{
        "status_cd": 200,
         "data" : {
                "message":true
          }
  }`

#### Fail 
`{
       "status_cd": 400,
        "error" : {
               "error_cd" : 400,
               "message" : "Error in verifying user identity"
         }
}`
### Description
      To verify whether the user is registered or not

# To verify whether the property is registered or not

> Endpoint     : api/v1/isProperty

> Request type : POST

| Key                    | Value          | Comment          |
| :-----------           | :------:       | :------:         | 
|propertyId|1|

### Responses
#### Success

 `{
        "status_cd": 200,
         "data" : {
                 "message":true
          }
  }`

#### Fail 
`{
       "status_cd": 400,
       "error" : {
             "error_cd" : 400,
             "message" : "Error in verifying property identity"
        }
}`
### Description
      To verify whether the property is registered or not

# To get user's addresses

> Endpoint     : api/v1/getUsersAddress

> Request type : POST

| Key                    | Value          | Comment          |
| :-----------           | :------:       | :------:         | 
|id|0|

### Responses
#### Success

 `{
         "status_cd": 200,
          "data" : {
                  "userAddress":0x549b6c3ba891530bba3856b2adb283c9c4045d64
           }
  }`

#### Fail -Invalid Index
`{
       "status_cd": 200,
        "data" : {
               "message":"Invalid index"
         }
}`
#### Fail 
`{
       "status_cd": 400,
       "error" : {
                 "error_cd" : 400,
                 "message" : "Error in fetching the user address"
         }
}`

### Description
      To get user's addresses

# To get property based on index

> Endpoint     : api/v1/getPropertyAtIndex


> Request type : POST

| Key                    | Value          | Comment          |
| :-----------           | :------:       | :------:         | 
|id|0|

### Responses
#### Success

 `{
         "status_cd": 200,
          "data" : {
                  "propertyId":1
           }
  }`

#### Fail 
`{
       "status_cd": 400,
       "error" : {
                 "error_cd" : 400,
                 "message" : "Error in fetching the property"
         }
}`

### Description
      To get the property by index.



# To get user's property count

> Endpoint     : api/v1/getUserPropertyCount

> Request type : POST

| Key                    | Value          | Comment          |
| :-----------           | :------:       | :------:         | 
|useraddress|0x549b6c3ba891530bba3856b2adb283c9c4045d64|

### Responses
#### Success

 `{
        
          "status_cd": 200,
           "data" : {
                 "count":2
            }
                           
  }`


#### Fail
`{
           "status_cd": 400,
           "error" : {
                    "error_cd" : 400,
                    "message" : "Error in fetching the property count for user"
           }
}`
### Description
      To get user's property count




# To get user's property id

> Endpoint     : api/v1/getUserPropertyId

> Request type : POST

| Key                    | Value          | Comment          |
| :-----------           | :------:       | :------:         | 
|id|0||
|useraddress|0x549b6c3ba891530bba3856b2adb283c9c4045d64||


### Responses
#### Success

 `{
          "status_cd": 200,
           "data" : {
                "propertyId":1
            }
  }`

#### Fail -Invalid Index
`{
       "status_cd": 200,
        "data" : {
               "message":"Invalid index"
         }
}`
#### Fail 
`{
       "status_cd": 400,
       "error" : {
                 "error_cd" : 400,
                 "message" : "Error in fetching the property id of user"
         }
}`
### Description
      To get user's property id.



# To get user details

> Endpoint     : api/v1/getUserDetails

> Request type : POST

| Key                    | Value          | Comment          |
| :-----------           | :------:       | :------:         | 
|userAddress|0x549b6c3ba891530bba3856b2adb283c9c4045d64|


### Responses
#### Success

 `{
        "status_cd": 200,
        "data": {
               result[
                     "Aksay Kumar",
                     "55"
                     "Business"
                ]
              
        }
  }`
#### Fail
`{
           "status_cd": 400,
           "error" : {
                    "error_cd" : 400,
                    "message" : "Error in fetching the user details"
           }
}`
### Description
      To get user details.



# To get user property details

> Endpoint     : api/v1/getUserPropertyDetails

> Request type : POST

| Key                    | Value          | Comment          |
| :-----------           | :------:       | :------:         | 
|propertyId|1|

### Responses
#### Success

 `{
        "status_cd": 200,
        "data": {
               result[
                      "11",
                      "12"
                ]
        }
  }`


#### Fail
`{
           "status_cd": 400,
           "error" : {
                    "error_cd" : 400,
                    "message" : "Error in fetching the property details"
           }
}`
### Description
      To get user property details based on propertyId

# To create Resident

> Endpoint     : api/v1/createResident

> Request type : POST

| Key                    | Value          | Comment          |
| :-----------           | :------:       | :------:         | 
|userAddress|0x549b6c3ba891530bba3856b2adb283c9c4045d64||
|name|Akshay Kumar||
|age|54||
|occupation|Business||
|password|Test12345||
|contactNumber||
|email||
|emiratesId||


### Responses
#### Success

 `{
        "status_cd": 200,
        "data": {
               "transactionHash": "0xd2dd8913da501a513cdaf26f94dc8161b331d62dbf5a4b6a1216f79e795c6313"
        }
  }`
#### Fail -Already Existing user
`{
       "status_cd": 200,
       "data" : {
             "message":"Already registered user"
        }
}`
#### Fail -Wrong Password Condition
`{
       "status_cd": 400,
        "error" : {
              "error_cd" : 400,
              "message" : "Error in Authentication"
        }
}`
#### Fail
`{
           "status_cd": 400,
           "error" : {
                    "error_cd" : 400,
                    "message" : "Error in creating resident"
           }
}`
### Description
      To create resident.


# To create property

> Endpoint     : api/v1/createProperty

> Request type : POST

| Key                    | Value          | Comment          |
| :-----------           | :------:       | :------:         | 
|userAddress|0x549b6c3ba891530bba3856b2adb283c9c4045d64||
|propertyId|1|expect int value|
|sqfeet|11||
|totalArea|12||
|password|Test12345||
|gpsCoordintes||
|landmark||

### Responses
#### Success

 `{
        "status_cd": 200,
        "data": {
               "transactionHash": "0xd2dd8913da501a513cdaf26f94dc8161b331d62dbf5a4b6a1216f79e795c6313"
        }
  }`
#### Fail -Already Existing propertyId
`{
       "status_cd": 200,
       "data" : {
             "message":"Already Registered with propertyId"
        }
}`
#### Fail -Wrong Password Condition
`{
       "status_cd": 400,
        "error" : {
              "error_cd" : 400,
              "message" : "Error in Authentication"
        }
}`
#### Fail
`{
           "status_cd": 400,
           "error" : {
                    "error_cd" : 400,
                    "message" : "Error in creating property"
           }
}`
### Description
      To create property.




# To update Resident

> Endpoint     : api/v1/updateResident

> Request type : POST

| Key                    | Value          | Comment          |
| :-----------           | :------:       | :------:         | 
|userAddress|0x549b6c3ba891530bba3856b2adb283c9c4045d64||
|name|Akshay Kumar||
|age|54||
|occupation|Business||
|password|Test12345||
|contactNumber||
|email||
|emiratesId||


### Responses
#### Success

 `{
        "status_cd": 200,
        "data": {
               "transactionHash": "0xd2dd8913da501a513cdaf26f94dc8161b331d62dbf5a4b6a1216f79e795c6313"
        }
  }`
#### Fail -Invalid params
`{
         "status_cd": 200,
          "data" : {
              "message":"Invalid data"
           }
}`

#### Fail -Wrong Password Condition
`{
       "status_cd": 400,
        "error" : {
              "error_cd" : 400,
              "message" : "Error in Authentication"
        }
}`
#### Fail
`{
           "status_cd": 400,
            "error" : {
                 "error_cd" : 400,
                 "message" : "Error in updating resident"
             }
}`
### Description
      To update resident.


# To update property

> Endpoint     : api/v1/updateProperty

> Request type : POST

| Key                    | Value          | Comment          |
| :-----------           | :------:       | :------:         | 
|userAddress|0x549b6c3ba891530bba3856b2adb283c9c4045d64||
|propertyId|1|expect int value|
|sqfeet|11||
|totalArea|12||
|password|Test12345||
|gpsCoordintes||
|landmark||

### Responses
#### Success

 `{
        "status_cd": 200,
        "data": {
               "transactionHash": "0xd2dd8913da501a513cdaf26f94dc8161b331d62dbf5a4b6a1216f79e795c6313"
        }
  }`
#### Fail -Invalid params
`{
         "status_cd": 200,
          "data" : {
              "message":"Invalid data"
           }
}`

#### Fail -Wrong Password Condition
`{
       "status_cd": 400,
        "error" : {
              "error_cd" : 400,
              "message" : "Error in Authentication"
        }
}`
#### Fail
`{
           "status_cd": 400,
            "error" : {
                 "error_cd" : 400,
                 "message" : "Error in updating property"
             }
}`
### Description
      To update property.


















# To property request by new user/buyer

> Endpoint     : api/v1/buyRequest

> Request type : POST

| Key                    | Value          | Comment          |
| :-----------           | :------:       | :------:         | 
|currentUserAddress|0x549b6c3ba891530bba3856b2adb283c9c4045d64||
|propertyId|1|expect int value|
|newOwnerAddress|0x549b6c3ba891530bba3856b2adb283c9c4045d64||
|password|Test12345||

### Responses
#### Success

 `{
        "status_cd": 200,
        "data": {
               "transactionHash": "0xd2dd8913da501a513cdaf26f94dc8161b331d62dbf5a4b6a1216f79e795c6313"
        }
  }`

#### Fail -Wrong Password Condition
`{
       "status_cd": 400,
        "error" : {
              "error_cd" : 400,
              "message" : "Error in Authentication"
        }
}`
#### Fail
`{
           "status_cd": 400,
            "error" : {
                 "error_cd" : 400,
                 "message" : "Error in adding"
             }
}`
### Description
      To request for buying the property by user/buyer.



# To notify user based on address

> Endpoint     : api/v1/buyRequestCheck

> Request type : POST

| Key                    | Value          | Comment          |
| :-----------           | :------:       | :------:         | 
|userAddress|0x549b6c3ba891530bba3856b2adb283c9c4045d64||


### Responses
#### Success

 `{
         "status_cd": 200,
         "data": {
             "result": [
                 "0xa1333f75a6cc851f5d93868cc824c25ce65803b9",
                 "1"
             ]
         }
  }`

#### Fail
`{
           "status_cd": 400,
            "error" : {
                 "error_cd" : 400,
                 "message" : "Error in fetching buyrequestcheck"
             }
}`
### Description
      To notify the request at user side.


# To confirm the buyer request by user(seller)

> Endpoint     : api/v1/buyRequestUpdate

> Request type : POST

| Key                    | Value          | Comment          |
| :-----------           | :------:       | :------:         | 
|userAddress|0x549b6c3ba891530bba3856b2adb283c9c4045d64||
|password|Test12345||

### Responses
#### Success

 `{
        "status_cd": 200,
        "data": {
               "transactionHash": "0xd2dd8913da501a513cdaf26f94dc8161b331d62dbf5a4b6a1216f79e795c6313"
        }
  }`

#### Fail -Wrong Password Condition
`{
       "status_cd": 400,
        "error" : {
              "error_cd" : 400,
              "message" : "Error in Authentication"
        }
}`
#### Fail
`{
           "status_cd": 400,
            "error" : {
                 "error_cd" : 400,
                 "message" : "Error in adding confirmation in buyRequest by user"
             }
}`
### Description
      To confirm the buyer request by user(seller)


# To get the transfer request count

> Endpoint     : api/v1/getRequestCount

> Request type : GET


### Responses
#### Success



  `{
         "status_cd": 200,
          "data" : {
                   "count":2
           }
  }`

#### Fail
`{
        "status_cd": 400,
         "error" : {
                       "error_cd" : 400,
                        "message" : "Error in fetching the user count"
          }
}`
### Description
          To get the transfer request count

# To get user's addresses of transfer request.

> Endpoint     : api/v1/getRequestAddressAtIndex

> Request type : POST

| Key                    | Value          | Comment          |
| :-----------           | :------:       | :------:         | 
|id|0|

### Responses
#### Success

 `{
         "status_cd": 200,
          "data" : {
                  "userAddress":0x549b6c3ba891530bba3856b2adb283c9c4045d64
           }
  }`

#### Fail -Invalid Index
`{
       "status_cd": 200,
        "data" : {
               "message":"Invalid index"
         }
}`
#### Fail 
`{
       "status_cd": 400,
       "error" : {
                 "error_cd" : 400,
                 "message" : "Error in fetching the requested user address"
         }
}`

### Description
      To get the requested user's addresses for land transfer(list the current owners of land transfer).




















# To view transfer request by admin

> Endpoint     : api/v1/buyRequestConfirm

> Request type : POST

| Key                    | Value          | Comment          |
| :-----------           | :------:       | :------:         | 
|userAddress|0x549b6c3ba891530bba3856b2adb283c9c4045d64||


### Responses
#### Success

 `{
         "status_cd": 200,
         "data": {
              "result": [
                   "0xa1333f75a6cc851f5d93868cc824c25ce65803b9",
                    "1"
               ]
          }
        
  }`

#### Fail
`{
           "status_cd": 400,
            "error" : {
                 "error_cd" : 400,
                 "message" : "Error in fetching buyrequestconfirm by admin"
             }
}`
### Description
      To update property.



# To execute transfer request by admin

> Endpoint     : api/v1/update

> Request type : POST

| Key                    | Value          | Comment          |
| :-----------           | :------:       | :------:         | 
|newOwnerAddress|0x549b6c3ba891530bba3856b2adb283c9c4045d64||
|propertyId|1|expect int value|
|password|Test12345||

### Responses
#### Success

 `{
        "status_cd": 200,
        "data": {
               "transactionHash": "0xd2dd8913da501a513cdaf26f94dc8161b331d62dbf5a4b6a1216f79e795c6313"
        }
  }`
#### Fail -Invalid params
`{
         "status_cd": 200,
          "data" : {
              "message":"Invalid data"
           }
}`

#### Fail -Wrong Password Condition
`{
       "status_cd": 400,
        "error" : {
              "error_cd" : 400,
              "message" : "Error in Authentication"
        }
}`
#### Fail
`{
           "status_cd": 400,
            "error" : {
                 "error_cd" : 400,
                 "message" : "Error in transfer by admin"
             }
}`
### Description
      To transfer property by admin.







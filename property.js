const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const config = require('./config.js');

let accountSchema = new Schema({
        address: String
    },
    {strict: false}
);


let proCollection = mongoose.model(config.collections.propertyCollection, accountSchema);

module.exports = proCollection;


module.exports = {
  networks: {
    development: {
      host: '127.0.0.1',
      port: 22000,
      network_id: '*', // Match any network id
      gas: 6000000,
      gasPrice: 0,
      from:"0x210f1e4c56d68f63ab4bf41157bbca253abfec45"
    }
  }
}

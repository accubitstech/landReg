
const express = require('express');
const devCollection = require('../../devOps');
const config = require('../../config.js');
const exec = require('child_process').exec;

const router = express.Router();

//To add node
router.post('/addNode', function (req, res) {

    var enode=req.body.enode;
    var type =req.body.typeNode;

    //Validation
    if(!type){
        res.send({
            "status_cd": 400,
            "error" : {
                "error_cd" : 400,
                "message" : "Please enter a valid type"
            }
        });
        return;
    }
    if(!enode){
        res.send({
            "status_cd": 400,
            "error" : {
                "error_cd" : 400,
                "message" : "Please enter a enode"
            }
        });
        return;
    }


    let DevCollection = new devCollection({
        enode: enode,
        type: type
    });
    DevCollection.save((err, data) => {
        if (err) {
            res.send({
                "status_cd": 400,
                "error" : {
                    "error_cd" : 400,
                    "message" : "Error in adding nodes"
                }
            });
        }

        res.send({
            "status_cd": 200,
            "data" : {
                "output":"Successfully added"
            }
        });

    });


});

router.get('/getBootNodes', function (req, res) {

    devCollection.find({"type":"1"}).lean().exec(function (err, out) {


        if (err) {
            return res.send({
                success: false,
                result: err.toString()
            });
        }
        if (!out) {
            return res.send({
                success: false,
                result: "No bootnodes available"
            });
        }
        else {
            let eno = [];
            out.forEach(function (o) {
                eno.push(o.enode)
            });

            res.send(eno);
        }
    });
});

router.get('/addChildNode', function (req, res) {


    var yourscript = exec('sh hi.sh',
        (error, stdout, stderr) => {
            console.log(`${stdout}`);
            console.log(`${stderr}`);
            if (error !== null) {
                console.log(`exec error: ${error}`);
            }
        });
});

module.exports = router;
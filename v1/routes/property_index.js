
const express = require('express');
const router = express.Router();
const config = require('../../config.js');
const proCollection = require('../../property.js');

const myAppProperty = require('../../app/javascripts/app-Property.js');

       
        //PropertyTransfer Contract Functions
       
        //To get the count of users 
        router.get('/getUserCount', function (req, res) {
                myAppProperty.getUserCount(function(result){
                       res.send(result);
                 });
        });


router.get('/getPropertyList', function (req, res) {
    proCollection.find({}).lean().exec(function (err, out) {


        if (err) {
            return res.send({
                success: false,
                result: err.toString()
            });
        }
        if (!out) {
            return res.send({
                success: false,
                result: "No properties available"
            });
        }
        else {

            res.send({
                success: true,
                result: out
            });
        }
    });
});


router.post('/getUserPropertyList', function (req, res) {
    let address = req.body.address;
    if(!address){
        res.send({
            "status_cd": 400,
            "error" : {
                "error_cd" : 400,
                "message" : "Please enter a valid address"
            }
        });
        return;
    }
    proCollection.find({'address':{$ne:address}}).lean().exec(function (err, out) {


        if (err) {
            return res.send({
                success: false,
                result: err.toString()
            });
        }
        if (!out) {
            return res.send({
                success: false,
                result: "No properties available"
            });
        }
        else {

            res.send({
                success: true,
                result: out
            });
        }
    });
});

        //To get the count of property
        router.get('/getPropertyCount', function (req, res) {
                myAppProperty.getPropertyCount(function(result){
                       res.send(result);
                 });
         });
        //To verify the the user address
        router.post('/isUser', function (req, res) {
                // var userAddress=req.body.userAddress;
                myAppProperty.isUser(req.body,function(result){
                        res.send(result);
               });
        });
        //To verify the the property id
        router.post('/isProperty', function (req, res) {
                // var propertyId=req.body.propertyId;
                myAppProperty.isProperty(req.body,function(result){
                        res.send(result);
               });
        });
        //To get the the users address by id
        router.post('/getUsersAddress', function (req, res) {
                // var id=req.body.id;
                myAppProperty.getUsersAddress(req.body,function(result){
                        res.send(result);
               });
        });
        //To get the count of properties for each user
        router.post('/getUserPropertyCount', function (req, res) {
                // var userAddress=req.body.userAddress;
                myAppProperty.getUserPropertyCount(req.body,function(result){
                        res.send(result);
               });
        });
        //To get the property by id
        router.post('/getPropertyAtIndex', function (req, res) {
                // var id=req.body.id;
                myAppProperty.getPropertyAtIndex(req.body,function(result){
                        res.send(result);
               });
        });
       //To get the property id of each user
       router.post('/getUserPropertyId', function (req, res) {
                  //  var address=req.body.userAddress;
                  //  var id = req.body.id;
                  myAppProperty.getUserPropertyId(req.body,function(result){
                           res.send(result);
                  });
      });
       //To get the user details by userid/user address
       router.post('/getUserDetails', function (req, res) {
                //  var userAddress = req.body.userAddress;
                myAppProperty.getUserDetails(req.body,function(result){
                         res.send(result);
                });
      });
      //To get the property details by propertyid
      router.post('/getUserPropertyDetails', function (req, res) {
                  //  var propertyId = req.body.propertyId;
                  myAppProperty.getUserPropertyDetails(req.body,function(result){
                        res.send(result);
                  });
     });
     //To create the user
     router.post('/createResident', function (req, res) {
                  // var userAddress  = req.body.userAddress; 
                  // var name  = req.body.name;
                  // var age  = req.body.age;
                  // var occupation = req.body.occupation;
                  // var password = req.body.password;   
                  //new fields
                  //var emiratesId=req.body.emiratesId;
                  //var email=req.body.email;
                  //var contactNumber=req.body.contactNumber;
                  myAppProperty.createUser(req.body,function(result){
                       res.send(result);
                  });
    });
    //To create the property
    router.post('/createProperty', function (req, res) {
        //var propertyId=req.body.propertyId;
        // var userAddress  = req.body.userAddress; 
        // var sqfeet  = req.body.sqfeet;
        // var totalArea  = req.body.totalArea;
        // var password = req.body.password;
        //new fields
        // var gpsCoordinates=req.body.gpsCoordinates;
        // var landmark=req.body.landmark;
        myAppProperty.createProperty(req.body,function(result){
             res.send(result);
        });
    });
    //To create the user
    router.post('/updateResident', function (req, res) {
        // var userAddress  = req.body.userAddress; 
        // var name  = req.body.name;
        // var age  = req.body.age;
        // var occupation = req.body.occupation;
        // var password = req.body.password;
        //new fields
        //var emiratesId=req.body.emiratesId;
        //var email=req.body.email;
        //var contactNumber=req.body.contactNumber;
        myAppProperty.updateUser(req.body,function(result){
                 res.send(result);
           });
    });
    //To create the property
    router.post('/updateProperty', function (req, res) {
                //var propertyId=req.body.propertyId;
                // var userAddress  = req.body.userAddress; 
                // var sqfeet  = req.body.sqfeet;
                // var totalArea  = req.body.totalArea;
                // var password = req.body.password;
                //new fields
               // var gpsCoordinates=req.body.gpsCoordinates;
               // var landmark=req.body.landmark;
                myAppProperty.updateProperty(req.body,function(result){
                     res.send(result);
                });
   });
   //new functions
   router.post('/buyRequest', function (req, res) {
        //var propertyId=req.body.propertyId;
        // var userAddress  = req.body.currentUserAddress; 
        // var newOwnerAddress  = req.body.newOwnerAddress;
        // var password = req.body.password;
        myAppProperty.buyRequest(req.body,function(result){
             res.send(result);
        });
   });
   router.post('/buyRequestCheck', function (req, res) {
        // var userAddress  = req.body.userAddress; 
        myAppProperty.buyRequestCheck(req.body,function(result){
             res.send(result);
        });
  });
  router.get('/getRequestCount', function (req, res) {
       
        myAppProperty.getRequestCount(function(result){
             res.send(result);
        });
  });
  router.post('/getRequestAddressAtIndex', function (req, res) {
        //var id=req.body.id;
       
        myAppProperty.getRequestAddressAtIndex(req.body,function(result){
             res.send(result);
        });
  });
  router.post('/buyRequestUpdate', function (req, res) {
       
        // var userAddress  = req.body.userAddress; 
        // var password = req.body.password;
       
        myAppProperty.buyRequestUpdate(req.body,function(result){
             res.send(result);
        });
  });
  router.post('/buyRequestConfirm', function (req, res) {
        
         // var userAddress  = req.body.userAddress; 
       
        
         myAppProperty.buyRequestConfirm(req.body,function(result){
              res.send(result);
         });
   });
   router.post('/update', function (req, res) {
        
         // var newOwnerAddress  = req.body.newOwnerAddress; 
         // var password = req.body.password;
        
         myAppProperty.update(req.body,function(result){
              res.send(result);
         });
   });
   //To delete from requested User
   router.post('/deleteRequestedUser', function (req, res) {
    
       // var userAddress  = req.body.userAddress; 
       // var password = req.body.password;
    
        myAppProperty.deleteRequestedUser(req.body,function(result){
               res.send(result);
        });
   });
   module.exports = router;
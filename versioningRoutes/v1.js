
const express = require('express');
const router = express.Router();

let v1aAPIS = require('../v1/routes/createuser_index.js');
let v1bAPIS = require('../v1/routes/property_index.js');
let v1cAPIS = require('../v1/routes/user_index.js');
let v1dAPIS = require('../v1/routes/devOps_index.js');


router.use('/v1',v1aAPIS,v1bAPIS,v1cAPIS,v1dAPIS);


module.exports = router;